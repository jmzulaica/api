//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

var movimientosJSONv2 = require('./movimientosv2.json');

console.log('todo list RESTful API server started on: ' + port);

app.get('/',function(req,res){
  //res.send("Hola mundo desde Node.js");
  res.sendFile(path.join(__dirname, 'index.html'));
})

app.post('/', function(req,res){
  res.send("Hemos recibido su petición POST");
})

app.put('/', function(req,res){
  res.send("Hemos recibido su petición PUT");
})

app.delete('/', function(req,res){
  res.send("Hemos recibido su petición DELETE");
})


app.get('/Clientes',function(req,res){
  res.send("Aqui tiene a los clientes");
})

app.get('/Clientes/:idcliente',function(req,res){
  res.send("Aqui tiene al cliente: " + req.params.idcliente);
})

app.get('/V1/Movimientos',function(req,res){
  res.sendfile('movimientosv1.json');
})

app.get('/V2/Movimientos',function(req,res){
  res.json(movimientosJSONv2);
})

app.get('/V2/Movimientos/:id',function(req,res){
  console.log(req.params.id);
  res.send(movimientosJSONv2[req.params.id - 1]);
})

app.get('/V2/Movimientos/:id/:nombre',function(req,res){
  console.log(req.params);
  console.log(req.params.id);
  console.log(req.params.nombre);
  res.send("recibido");
})

//Query
app.get('/V2/Movimientosq',function(req,res){
  console.log(req.query);
  res.send("Petición con query recibida y cambiada: " + req.query);
})
